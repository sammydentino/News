//
//  Retriever.swift
//  News
//
//  Created by Sammy Dentino on 2/10/21.
//

import SwiftUI

class Retriever: ObservableObject {
    @Published var business: Results!
    @Published var entertainment: Results!
    @Published var general: Results!
    @Published var health: Results!
    @Published var science: Results!
    @Published var sports: Results!
    @Published var tech: Results!
    @Published var foxnews: Results!
    
    init() {
        loadAll()
    }
    
    func loadAll() {
        getFoxNews()
        getBusinessNews()
        getEntertainmentNews()
        //getGeneralNews()
        getHealthNews()
        getScienceNews()
        getSportsNews()
        getTechNews()
    }
    
    func getFoxNews() {
        let urlString = "https://saurav.tech/NewsAPI/everything/fox-news.json"
        
        if let data = try? JSONDecoder().decode(Results.self, from: Data(contentsOf: URL(string: urlString)!)) {
            foxnews = data
        }
    }
    
    func getHealthNews() {
        let urlString = "https://saurav.tech/NewsAPI/top-headlines/category/health/us.json"
        
        if let data = try? JSONDecoder().decode(Results.self, from: Data(contentsOf: URL(string: urlString)!)) {
            health = data
        }
    }
    
    func getBusinessNews() {
        let urlString = "https://saurav.tech/NewsAPI/top-headlines/category/business/us.json"
        
        if let data = try? JSONDecoder().decode(Results.self, from: Data(contentsOf: URL(string: urlString)!)) {
            business = data
        }
    }
    
    func getEntertainmentNews() {
        let urlString = "https://saurav.tech/NewsAPI/top-headlines/category/entertainment/us.json"
        
        if let data = try? JSONDecoder().decode(Results.self, from: Data(contentsOf: URL(string: urlString)!)) {
            entertainment = data
        }
    }
    
    func getGeneralNews() {
        let urlString = "https://saurav.tech/NewsAPI/top-headlines/category/general/us.json"
        
        if let data = try? JSONDecoder().decode(Results.self, from: Data(contentsOf: URL(string: urlString)!)) {
            general = data
        }
    }
    
    func getScienceNews() {
        let urlString = "https://saurav.tech/NewsAPI/top-headlines/category/science/us.json"
        
        if let data = try? JSONDecoder().decode(Results.self, from: Data(contentsOf: URL(string: urlString)!)) {
            science = data
        }
    }
    
    func getSportsNews() {
        let urlString = "https://saurav.tech/NewsAPI/top-headlines/category/sports/us.json"
        
        if let data = try? JSONDecoder().decode(Results.self, from: Data(contentsOf: URL(string: urlString)!)) {
            sports = data
        }
    }
    
    func getTechNews() {
        let urlString = "https://saurav.tech/NewsAPI/top-headlines/category/technology/us.json"
        
        if let data = try? JSONDecoder().decode(Results.self, from: Data(contentsOf: URL(string: urlString)!)) {
            tech = data
        }
    }
}

struct Results : Codable {
    let status : String!
    let totalResults : Int!
    let articles : [Articles]!

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case totalResults = "totalResults"
        case articles = "articles"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(String.self, forKey: .status) ?? "N/A"
        totalResults = try values.decodeIfPresent(Int.self, forKey: .totalResults) ?? 0
        articles = try values.decodeIfPresent([Articles].self, forKey: .articles)
    }
}

struct Articles : Codable, Identifiable {
    let id = UUID()
    let source : Source!
    let author : String!
    let title : String!
    let description : String!
    let url : String!
    let urlToImage : String!
    let publishedAt : String!
    let content : String!

    enum CodingKeys: String, CodingKey {
        case source = "source"
        case author = "author"
        case title = "title"
        case description = "description"
        case url = "url"
        case urlToImage = "urlToImage"
        case publishedAt = "publishedAt"
        case content = "content"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        source = try values.decodeIfPresent(Source.self, forKey: .source)
        author = try values.decodeIfPresent(String.self, forKey: .author) ?? "N/A"
        title = try values.decodeIfPresent(String.self, forKey: .title) ?? "N/A"
        description = try values.decodeIfPresent(String.self, forKey: .description) ?? ""
        url = try values.decodeIfPresent(String.self, forKey: .url) ?? "https://github.com/sammydentino/AQI-Definitions/raw/main/notfound.png"
        urlToImage = try values.decodeIfPresent(String.self, forKey: .urlToImage) ?? "https://github.com/sammydentino/AQI-Definitions/raw/main/notfound.png"
        publishedAt = try values.decodeIfPresent(String.self, forKey: .publishedAt) ?? "N/A"
        content = try values.decodeIfPresent(String.self, forKey: .content) ?? "N/A"
        //title = String(String(title.reversed()).components(separatedBy: "-")[1].reversed())
    }
}

struct Source : Codable {
    let id : String!
    let name : String!

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id) ?? "N/A"
        name = try values.decodeIfPresent(String.self, forKey: .name) ?? "N/A"
    }
}
