//
//  Extensions.swift
//  News
//
//  Created by Sammy Dentino on 2/10/21.
//

import SwiftUI
import WebKit

struct WebView : UIViewRepresentable {
    let request: URLRequest
      
    func makeUIView(context: Context) -> WKWebView  {
        let webview = WKWebView()
        webview.load(request)
        return webview
    }
      
    func updateUIView(_ webview: WKWebView, context: Context) {}
}

struct BlurView: UIViewRepresentable {
    func makeUIView(context: Context) -> UIVisualEffectView {
        return UIVisualEffectView(effect: UIBlurEffect(style: .systemChromeMaterial))
    }
    
    func updateUIView(_ uiView: UIVisualEffectView, context: Context) {
        
    }
}

extension View {
    //dismiss keyboard
    func hideKeyboard() {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
    
    //fixes capitalization bug in iOS 14
    @ViewBuilder func fixCase() -> some View {
        if #available(iOS 14.0, *) {
            self.textCase(.none)
        } else {
            self
        }
    }
    
    //fixes the different list styles for 13 & 14
    @ViewBuilder func fixList() -> some View {
        if #available(iOS 14.0, *) {
            self.listStyle(InsetGroupedListStyle())
        } else {
            self.listStyle(GroupedListStyle()).environment(\.horizontalSizeClass, .regular)
        }
    }
    
    //text style modifiers
    @ViewBuilder func tab(i: CGFloat) -> some View {
        self.font(.system(size: i, weight: .medium, design: .rounded))
    }
    @ViewBuilder func title() -> some View {
        self.font(.system(size: 28, weight: .bold, design: .rounded)).animation(.default).fixCase()
    }
    @ViewBuilder func subhead() -> some View {
        self.font(.system(size: 15, weight: .semibold, design: .rounded)).animation(.default).fixCase()
    }
    @ViewBuilder func head() -> some View {
        self.font(.system(size: 24, weight: .bold)).animation(.default).fixCase()
    }
    @ViewBuilder func caption() -> some View {
        self.font(.system(size: 12, weight: .bold, design: .rounded)).animation(.default).fixCase()
    }
    @ViewBuilder func textColor(i: Color) -> some View {
        self.foregroundColor(i)
    }
    @ViewBuilder func bgColor(i: Color) -> some View {
        self.background(i)
    }
    
    //padding shorthand functions
    @ViewBuilder func pad(i: CGFloat) -> some View {
        self.padding(i)
    }
    @ViewBuilder func hPad(i: CGFloat) -> some View {
        self.padding(.horizontal, i)
    }
    @ViewBuilder func vPad(i: CGFloat) -> some View {
        self.padding(.vertical, i)
    }
    @ViewBuilder func aPad(i: CGFloat) -> some View {
        self.padding(.all, i)
    }
    @ViewBuilder func tPad(i: CGFloat) -> some View {
        self.padding(.top, i)
    }
    @ViewBuilder func bPad(i: CGFloat) -> some View {
        self.padding(.bottom, i)
    }
    @ViewBuilder func lPad(i: CGFloat) -> some View {
        self.padding(.leading, i)
    }
    @ViewBuilder func rPad(i: CGFloat) -> some View {
        self.padding(.trailing, i)
    }
    
    //navigation view builders
    @ViewBuilder func makeNav() -> some View {
        NavigationView {
            self
        }.navigationViewStyle(StackNavigationViewStyle()).navigationBarColor(UIColor.myControlBackground)
    }
    @ViewBuilder func makeHomeNav() -> some View {
        NavigationView {
            self
        }.navigationViewStyle(StackNavigationViewStyle()).navigationBarColor(UIColor.myControlBackground).bPad(i: -7.5)
    }
    @ViewBuilder func makeRadarNav() -> some View {
        NavigationView {
            self.edgesIgnoringSafeArea(.bottom)
        }.navigationViewStyle(StackNavigationViewStyle()).navigationBarColor(UIColor.myControlBackground)
    }
    
    @ViewBuilder func makeVStack() -> some View {
        if #available(iOS 14.0, *) {
            LazyVStack(alignment: .leading, spacing: 0) {
                self
            }
        } else {
            VStack(alignment: .leading, spacing: 0) {
                self
            }
        }
    }
    
    //list section builders
    @ViewBuilder func makeSection(str: String, color: Color) -> some View {
        if #available(iOS 14.0, *) {
            Section(header: Text("\(str)").head().foregroundColor(color)) {
                self
            }
        } else {
            Section(header: Text("\(str)").head().foregroundColor(color)) {
                self
            }
        }
    }
    @ViewBuilder func makeNewLineSection(str: String, color: Color) -> some View {
        if #available(iOS 14.0, *) {
            Section(header: Text("\n\(str)").head().foregroundColor(color).padding(.top, -15)) {
                self
            }
        } else {
            Section(header: Text("\n\(str)").head().foregroundColor(color)) {
                self
            }
        }
    }
    @ViewBuilder func makeNewLineSectionBoth(str: String, str2: String) -> some View {
        if #available(iOS 14.0, *) {
            Section(header: Text("\n   \(str)").subhead(), footer: Text("   \(str2)").caption()) {
                self
            }
        } else {
            Section(header: Text("\n\(str)").subhead(), footer: Text("\(str2)").caption()) {
                self
            }
        }
    }
    @ViewBuilder func makeDarkSection(str: String) -> some View {
        if #available(iOS 14.0, *) {
            Section(header: Text("   \(str)").subhead().foregroundColor(.primary)) {
                self
            }
        } else {
            Section(header: Text("\(str)").subhead().foregroundColor(.primary)) {
                self
            }
        }
    }
    @ViewBuilder func makeDarkNewLineSection(str: String) -> some View {
        if #available(iOS 14.0, *) {
            Section(header: Text("\n   \(str)").subhead().foregroundColor(.primary)) {
                self
            }
        } else {
            Section(header: Text("\n\(str)").subhead().foregroundColor(.primary)) {
                self
            }
        }
    }
    @ViewBuilder func makeEmptySection() -> some View {
        Section(header: Text(" ")) {
            self
        }
    }
    
    //navigation bar title setters
    @ViewBuilder func sTitle(str: String) -> some View {
        self.navigationBarTitle("\(str)", displayMode: .inline)
    }
    @ViewBuilder func lTitle(str: String) -> some View {
        self.navigationBarTitle("\(str)", displayMode: .large)
    }
    
    //navigation stack style setter
    @ViewBuilder func navStack() -> some View {
        self.navigationViewStyle(StackNavigationViewStyle())
    }
    
    @ViewBuilder func navButton(actionin: (() -> Void)?, str: String) -> some View {
        self.navigationBarItems(leading:
            Button(action: {
                    actionin?()
            }) {
                Text(str).subhead().textColor(i: .secondary)
            })
    }
    
    //set nav bar color
    func navigationBarColor(_ backgroundColor: UIColor?) -> some View {
        self.modifier(NavigationBarModifier(backgroundColor: backgroundColor))
    }
}

//required struct for the navigationBarColor View extension func
struct NavigationBarModifier: ViewModifier {
    var backgroundColor: UIColor?
    
    init( backgroundColor: UIColor?) {
        self.backgroundColor = backgroundColor
        let coloredAppearance = UINavigationBarAppearance()
        coloredAppearance.backgroundColor = UIColor.myControlBackground
        coloredAppearance.titleTextAttributes = [.foregroundColor: UIColor.myControlText]
        coloredAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.myControlText]
        
        UINavigationBar.appearance().standardAppearance = coloredAppearance
        UINavigationBar.appearance().compactAppearance = coloredAppearance
        UINavigationBar.appearance().scrollEdgeAppearance = coloredAppearance
        UINavigationBar.appearance().tintColor = .systemTeal
    }
    
    func body(content: Content) -> some View {
        ZStack{
            content
            VStack {
                GeometryReader { geometry in
                    Color(self.backgroundColor ?? .clear)
                        .frame(height: geometry.safeAreaInsets.top)
                        .edgesIgnoringSafeArea(.top)
                    Spacer()
                }
            }
        }
    }
}

extension UIFont {
    func withTraits(traits: UIFontDescriptor.SymbolicTraits) -> UIFont {
        let descriptor = fontDescriptor.withSymbolicTraits(traits)
        return UIFont(descriptor: descriptor!, size: 0) //size 0 means keep the size as it is
    }

    func bold() -> UIFont {
        return withTraits(traits: .traitBold)
    }

    func italic() -> UIFont {
        return withTraits(traits: .traitItalic)
    }
}

extension BinaryInteger {
    var binaryDescription: String {
        var binaryString = ""
        var internalNumber = self
        var counter = 0

        for _ in (1...self.bitWidth) {
            binaryString.insert(contentsOf: "\(internalNumber & 1)", at: binaryString.startIndex)
            internalNumber >>= 1
            counter += 1
            if counter % 4 == 0 {
                binaryString.insert(contentsOf: " ", at: binaryString.startIndex)
            }
        }
        return binaryString
    }
}

extension UIDevice {
    var hasNotch: Bool {
        let bottom = UIApplication.shared.windows.filter { $0.isKeyWindow }.first?.safeAreaInsets.bottom ?? 0
        return bottom > 0
    }
}

extension UIColor {
    static let myControlBackground: UIColor = dynamicColor(light: UIColor.white, dark: UIColor(red: 44/255, green: 44/255, blue: 46/255, alpha: 1.0))
    static let myTabBackground: UIColor = dynamicColor(light: UIColor.white, dark: .black)
    static let myControlText: UIColor = dynamicColor(light: UIColor.black, dark: UIColor.white)
    static func dynamicColor(light: UIColor, dark: UIColor) -> UIColor {
        return UIColor { $0.userInterfaceStyle == .dark ? dark : light }
    }
    public convenience init?(hex: String) {
        let r, g, b, a: CGFloat

        if hex.hasPrefix("#") {
            let start = hex.index(hex.startIndex, offsetBy: 1)
            let hexColor = String(hex[start...])
            if hexColor.count == 8 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0
                if scanner.scanHexInt64(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255
                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            }
        }
        return nil
    }
}

extension UIImageView {
    public func loadGif(name: String) {
        DispatchQueue.global().async {
            let image = UIImage.gif(name: name)
            DispatchQueue.main.async {
                self.image = image
            }
        }
    }

    @available(iOS 9.0, *)
    public func loadGif(asset: String) {
        DispatchQueue.global().async {
            let image = UIImage.gif(asset: asset)
            DispatchQueue.main.async {
                self.image = image
            }
        }
    }
}

extension Image {
    func themeImageModifier() -> some View {
        self.resizable().clipped().frame(width: 40, height: 40)
    }
    func themeImageModifierLarge() -> some View {
        self.resizable().clipped().frame(width: 75, height: 75)
    }
    func dayThemeImageModifier() -> some View {
        self.resizable().clipped().frame(width: 45, height: 45)
    }
    func sunriseImageModifier() -> some View {
        self.resizable().clipped().frame(width: 25, height: 25)
    }
}

extension Color {
    static let lightText = Color(UIColor.lightText)
    static let darkText = Color(UIColor.darkText)
    static let placeholderText = Color(UIColor.placeholderText)

    static let label = Color(UIColor.label)
    static let secondaryLabel = Color(UIColor.secondaryLabel)
    static let tertiaryLabel = Color(UIColor.tertiaryLabel)
    static let quaternaryLabel = Color(UIColor.quaternaryLabel)

    static let systemBackground = Color(UIColor.systemBackground)
    static let secondarySystemBackground = Color(UIColor.secondarySystemBackground)
    static let tertiarySystemBackground = Color(UIColor.tertiarySystemBackground)

    static let systemFill = Color(UIColor.systemFill)
    static let secondarySystemFill = Color(UIColor.secondarySystemFill)
    static let tertiarySystemFill = Color(UIColor.tertiarySystemFill)
    static let quaternarySystemFill = Color(UIColor.quaternarySystemFill)

    static let systemGroupedBackground = Color(UIColor.systemGroupedBackground)
    static let secondarySystemGroupedBackground = Color(UIColor.secondarySystemGroupedBackground)
    static let tertiarySystemGroupedBackground = Color(UIColor.tertiarySystemGroupedBackground)

    static let systemGray = Color(UIColor.systemGray)
    static let systemGray2 = Color(UIColor.systemGray2)
    static let systemGray3 = Color(UIColor.systemGray3)
    static let systemGray4 = Color(UIColor.systemGray4)
    static let systemGray5 = Color(UIColor.systemGray5)
    static let systemGray6 = Color(UIColor.systemGray6)

    static let separator = Color(UIColor.separator)
    static let opaqueSeparator = Color(UIColor.opaqueSeparator)
    static let link = Color(UIColor.link)

    static var systemRed: Color { return Color(UIColor.systemRed) }
    static var systemBlue: Color { return Color(UIColor.systemBlue) }
    static var systemPink: Color { return Color(UIColor.systemPink) }
    static var systemTeal: Color { return Color(UIColor.systemTeal) }
    static var systemGreen: Color { return Color(UIColor.systemGreen) }
    static var systemIndigo: Color { return Color(UIColor.systemIndigo) }
    static var systemOrange: Color { return Color(UIColor.systemOrange) }
    static var systemPurple: Color { return Color(UIColor.systemPurple) }
    static var systemYellow: Color { return Color(UIColor.systemYellow) }
}

extension UIImage {
    public class func gif(data: Data) -> UIImage? {
        // Create source from data
        guard let source = CGImageSourceCreateWithData(data as CFData, nil) else {
            print("SwiftGif: Source for the image does not exist")
            return nil
        }

        return UIImage.animatedImageWithSource(source)
    }

    public class func gif(url: String) -> UIImage? {
        // Validate URL
        guard let bundleURL = URL(string: url) else {
            print("SwiftGif: This image named \"\(url)\" does not exist")
            return nil
        }

        // Validate data
        guard let imageData = try? Data(contentsOf: bundleURL) else {
            print("SwiftGif: Cannot turn image named \"\(url)\" into NSData")
            return nil
        }

        return gif(data: imageData)
    }

    public class func gif(name: String) -> UIImage? {
        // Check for existance of gif
        guard let bundleURL = Bundle.main
          .url(forResource: name, withExtension: "gif") else {
            print("SwiftGif: This image named \"\(name)\" does not exist")
            return nil
        }

        // Validate data
        guard let imageData = try? Data(contentsOf: bundleURL) else {
            print("SwiftGif: Cannot turn image named \"\(name)\" into NSData")
            return nil
        }

        return gif(data: imageData)
    }

    @available(iOS 9.0, *)
    public class func gif(asset: String) -> UIImage? {
        // Create source from assets catalog
        guard let dataAsset = NSDataAsset(name: asset) else {
            print("SwiftGif: Cannot turn image named \"\(asset)\" into NSDataAsset")
            return nil
        }

        return gif(data: dataAsset.data)
    }

    internal class func delayForImageAtIndex(_ index: Int, source: CGImageSource!) -> Double {
        var delay = 0.1

        // Get dictionaries
        let cfProperties = CGImageSourceCopyPropertiesAtIndex(source, index, nil)
        let gifPropertiesPointer = UnsafeMutablePointer<UnsafeRawPointer?>.allocate(capacity: 0)
        defer {
            gifPropertiesPointer.deallocate()
        }
        let unsafePointer = Unmanaged.passUnretained(kCGImagePropertyGIFDictionary).toOpaque()
        if CFDictionaryGetValueIfPresent(cfProperties, unsafePointer, gifPropertiesPointer) == false {
            return delay
        }

        let gifProperties: CFDictionary = unsafeBitCast(gifPropertiesPointer.pointee, to: CFDictionary.self)

        // Get delay time
        var delayObject: AnyObject = unsafeBitCast(
            CFDictionaryGetValue(gifProperties,
                Unmanaged.passUnretained(kCGImagePropertyGIFUnclampedDelayTime).toOpaque()),
            to: AnyObject.self)
        if delayObject.doubleValue == 0 {
            delayObject = unsafeBitCast(CFDictionaryGetValue(gifProperties,
                Unmanaged.passUnretained(kCGImagePropertyGIFDelayTime).toOpaque()), to: AnyObject.self)
        }

        if let delayObject = delayObject as? Double, delayObject > 0 {
            delay = delayObject
        } else {
            delay = 0.1 // Make sure they're not too fast
        }

        return delay
    }

    internal class func gcdForPair(_ lhs: Int?, _ rhs: Int?) -> Int {
        var lhs = lhs
        var rhs = rhs
        // Check if one of them is nil
        if rhs == nil || lhs == nil {
            if rhs != nil {
                return rhs!
            } else if lhs != nil {
                return lhs!
            } else {
                return 0
            }
        }

        // Swap for modulo
        if lhs! < rhs! {
            let ctp = lhs
            lhs = rhs
            rhs = ctp
        }

        // Get greatest common divisor
        var rest: Int
        while true {
            rest = lhs! % rhs!

            if rest == 0 {
                return rhs! // Found it
            } else {
                lhs = rhs
                rhs = rest
            }
        }
    }

    internal class func gcdForArray(_ array: [Int]) -> Int {
        if array.isEmpty {
            return 1
        }

        var gcd = array[0]

        for val in array {
            gcd = UIImage.gcdForPair(val, gcd)
        }

        return gcd
    }

    internal class func animatedImageWithSource(_ source: CGImageSource) -> UIImage? {
        let count = CGImageSourceGetCount(source)
        var images = [CGImage]()
        var delays = [Int]()

        // Fill arrays
        for index in 0..<count {
            // Add image
            if let image = CGImageSourceCreateImageAtIndex(source, index, nil) {
                images.append(image)
            }

            // At it's delay in cs
            let delaySeconds = UIImage.delayForImageAtIndex(Int(index),
                source: source)
            delays.append(Int(delaySeconds * 1000.0)) // Seconds to ms
        }

        // Calculate full duration
        let duration: Int = {
            var sum = 0

            for val: Int in delays {
                sum += val
            }

            return sum
            }()

        // Get frames
        let gcd = gcdForArray(delays)
        var frames = [UIImage]()

        var frame: UIImage
        var frameCount: Int
        for index in 0..<count {
            frame = UIImage(cgImage: images[Int(index)])
            frameCount = Int(delays[Int(index)] / gcd)

            for _ in 0..<frameCount {
                frames.append(frame)
            }
        }

        let animation = UIImage.animatedImage(with: frames,
            duration: Double(duration) / 1000.0)

        return animation
    }
}

class GIFPlayerView: UIView {
    private let imageView = UIImageView()

    convenience init(gifName: String) {
       self.init()
       let gif = UIImage.gif(asset: gifName)
       imageView.image = gif
       imageView.contentMode = .center
       self.addSubview(imageView)
    }

    override init(frame: CGRect) {
       super.init(frame: frame)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        imageView.frame = bounds
    }
}

struct GIFView: UIViewRepresentable {
    var gifName: String

    func updateUIView(_ uiView: UIView, context: UIViewRepresentableContext<GIFView>) {

    }


    func makeUIView(context: Context) -> UIView {
        return GIFPlayerView(gifName: gifName)
    }
}

@available(iOS 14.0, *)
extension Color {
    var components: (red: CGFloat, green: CGFloat, blue: CGFloat, opacity: CGFloat) {
        typealias NativeColor = UIColor

        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var o: CGFloat = 0

        guard NativeColor(self).getRed(&r, green: &g, blue: &b, alpha: &o) else {
            // You can handle the failure here as you want
            return (0, 0, 0, 0)
        }
        return (r, g, b, o)
    }
}

extension UIScreen {
    static let screenWidth = UIScreen.main.bounds.width
    static let screenHeight = UIScreen.main.bounds.height
}
