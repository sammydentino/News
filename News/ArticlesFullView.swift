//
//  ArticlesFullView.swift
//  News
//
//  Created by Sammy Dentino on 2/10/21.
//

import SwiftUI
import Kingfisher
import BetterSafariView

struct ArticlesFullView: View {
    let data: Results
    let color: Color
    @State private var presentingSafariView = false
    
    var body: some View {
        List (data.articles!) { item in
            Button(action: {
                presentingSafariView = true
            }) {
                VStack (alignment: .leading){
                    HStack {
                        KFImage(URL(string: item.urlToImage ?? "https://github.com/sammydentino/AQI-Definitions/raw/main/notfound.png")!).placeholder({
                            Image("notfound")
                                .resizable()
                                .aspectRatio(contentMode: .fill)
                                .clipped()
                                .clipShape(Circle())
                                .frame(width: 55, height: 55)
                                .padding(EdgeInsets(top: 7.5, leading: 0, bottom: 0, trailing: 5))
                        })
                            .resizable()
                            .aspectRatio(contentMode: .fill)
                            .clipped()
                            .clipShape(Circle())
                            .frame(width: 55, height: 55)
                            .padding(EdgeInsets(top: 7.5, leading: 0, bottom: 0, trailing: 5))
                        Text(item.title!.components(separatedBy: " - ")[0])
                            .font(.subheadline)
                            .bold()
                            .foregroundColor(.primary)
                            .padding(EdgeInsets(top: 7.5, leading: 0, bottom: 0, trailing: 0))
                    }
                    Spacer()
                    Text(item.description!)
                        .font(.subheadline)
                        .foregroundColor(.gray)
                        .bold()
                        .lineLimit(3)
                        .padding(EdgeInsets(top: 5, leading: 0, bottom: 7.5, trailing: 0))
                }
            }.padding(EdgeInsets(top: 0, leading: 0, bottom: 7.5, trailing: 0))
            .sheet(isPresented: $presentingSafariView) {
                SafariView(
                    url: URL(string: item.url!)!,
                    configuration: SafariView.Configuration(
                        entersReaderIfAvailable: true,
                        barCollapsingEnabled: false
                    )
                )
                .preferredBarAccentColor(.white)
                .preferredControlAccentColor(color)
                .dismissButtonStyle(.done)
            }
        }.animation(.default)
    }
}
