//
//  ContentView.swift
//  News
//
//  Created by Sammy Dentino on 2/10/21.
//

import SwiftUI
import CoreData
import SwiftUIRefresh

struct ContentView: View {
    @ObservedObject var data = Retriever()
    @State private var selected = 0
    @State private var loading = false
    
    var body: some View {
        categories
            .pullToRefresh(isShowing: $loading) {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5, execute: {
                    data.loadAll()
                    self.loading = false
                })
                self.loading = false
            }
            .animation(.default)
    }
    
    var categories: some View {
        NavigationView {
            List {
                Group {
                    ArticlesView(data: data.business, str: "Business", color: .systemRed)
                    HStack {
                        Text("View all Business").subhead().foregroundColor(.systemRed)
                        Spacer()
                        Text("→").subhead().foregroundColor(.systemRed)
                    }.background(NavigationLink("", destination: ArticlesFullView(data: data.business, color: .systemRed).edgesIgnoringSafeArea(.bottom).navigationBarTitle(Text("Business"), displayMode: .inline)).opacity(0))
                }.makeNewLineSection(str: "Business", color: Color.systemRed)
                Group {
                    ArticlesView(data: data.entertainment, str: "Entertainment", color: .systemOrange)
                    HStack {
                        Text("View all Entertainment").subhead().foregroundColor(.systemOrange)
                        Spacer()
                        Text("→").subhead().foregroundColor(.systemOrange)
                    }.background(NavigationLink("", destination: ArticlesFullView(data: data.entertainment, color: .systemOrange).edgesIgnoringSafeArea(.bottom).navigationBarTitle(Text("Entertainment"), displayMode: .inline)))
                }.makeSection(str: "Entertainment", color: Color.systemOrange)
                //ArticlesView(data: data.general, str: "General").makeSection(str: "General")
                Group {
                    ArticlesView(data: data.health, str: "Health", color: .systemGreen)
                    HStack {
                        Text("View all Health").subhead().foregroundColor(.systemGreen)
                        Spacer()
                        Text("→").subhead().foregroundColor(.systemGreen)
                    }.background(NavigationLink("", destination: ArticlesFullView(data: data.health, color: .systemGreen).edgesIgnoringSafeArea(.bottom).navigationBarTitle(Text("Health"), displayMode: .inline)))
                }.makeSection(str: "Health", color: Color.systemGreen)
                Group {
                    ArticlesView(data: data.science, str: "Science", color: .systemBlue)
                    HStack {
                        Text("View all Science").subhead().foregroundColor(.systemBlue)
                        Spacer()
                        Text("→").subhead().foregroundColor(.systemBlue)
                    }.background(NavigationLink("", destination: ArticlesFullView(data: data.science, color: .systemGreen).edgesIgnoringSafeArea(.bottom).navigationBarTitle(Text("Science"), displayMode: .inline)))
                }.makeSection(str: "Science", color: Color.systemBlue)
                Group {
                    ArticlesView(data: data.sports, str: "Sports", color: .systemIndigo)
                    HStack {
                        Text("View all Sports").subhead().foregroundColor(.systemIndigo)
                        Spacer()
                        Text("→").subhead().foregroundColor(.systemIndigo)
                    }.background(NavigationLink("", destination: ArticlesFullView(data: data.sports, color: .systemBlue).edgesIgnoringSafeArea(.bottom).navigationBarTitle(Text("Sports"), displayMode: .inline)))
                }.makeSection(str: "Sports", color: Color.systemIndigo)
                Group {
                    ArticlesView(data: data.tech, str: "Technology", color: .systemPurple)
                    HStack {
                        Text("View all Technology").subhead().foregroundColor(.systemPurple)
                        Spacer()
                        Text("→").subhead().foregroundColor(.systemPurple)
                    }.background(NavigationLink("", destination: ArticlesFullView(data: data.tech, color: .systemPurple).edgesIgnoringSafeArea(.bottom).navigationBarTitle(Text("Technology"), displayMode: .inline)))
                }.makeSection(str: "Technology", color: Color.systemPurple)
                Group {
                    ArticlesView(data: data.foxnews, str: "Technology", color: .systemTeal)
                    HStack  {
                        Text("View all Fox News").subhead().foregroundColor(.systemTeal)
                        Spacer()
                        Text("→").subhead().foregroundColor(.systemTeal)
                    }.background(NavigationLink("", destination: ArticlesFullView(data: data.foxnews, color: .systemTeal).edgesIgnoringSafeArea(.bottom).navigationBarTitle(Text("Fox News"), displayMode: .inline)))
                }.makeSection(str: "Fox News", color: Color.systemTeal)
            }.listStyle(GroupedListStyle())
            .navigationBarTitle("News")
            .navigationViewStyle(StackNavigationViewStyle())
            .navigationBarColor(.myControlBackground)
            .animation(.default)
        }
    }
}
